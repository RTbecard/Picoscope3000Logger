#include "picologger.h"
#include <QApplication>
#include <QFont>
#include <QFontDatabase>
int main(int argc, char *argv[])
{

    QApplication a(argc, argv);

    // Add and set custom font
    QFontDatabase::addApplicationFont(":/CustomFont/OpenSans/OpenSans-Regular.ttf");
    QFont appFont = QFont("OpenSans", 9, 1);
    QApplication::setFont(appFont);

    PicoLogger w;

    // Launch GUI
    w.show();

    return a.exec();
}
