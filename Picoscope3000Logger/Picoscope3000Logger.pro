#-------------------------------------------------
#
# Project created by QtCreator 2017-04-15T00:33:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Picoscope3000Logger
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        picologger.cpp \
    logdata.cpp

HEADERS  += picologger.h \
    logdata.h

FORMS    += picologger.ui

## Make sure external resources (custom font) is compiled into the binary
RESOURCES += \
    fonts.qrc

# Link picoscope drivers in ubuntu
unix:LIBS += -L"/opt/picoscope/lib" -lps3000
unix:INCLUDEPATH += "/opt/picoscope/include/libps3000-3.7/"

# Link Picoscope drivers in windows
win32:LIBS += -L"C:/Program Files (x86)/Pico Technology/SDK/lib/" -lps3000
win32:INCLUDEPATH += "C:/Program Files (x86)/Pico Technology/SDK/inc/"
