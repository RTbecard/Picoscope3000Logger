#ifndef PICOLOGGER_H
#define PICOLOGGER_H

#include <QMainWindow>
#include "QString"
#include <string>

namespace Ui {
class PicoLogger;
}

class PicoLogger : public QMainWindow
{
    Q_OBJECT

public:
    explicit PicoLogger(QWidget *parent = 0);
    ~PicoLogger();
    bool loopLogging;

signals:
    void startLogging(short ,unsigned long, short,
                      unsigned long, unsigned long, short, PicoLogger*,
                      short, short, short, short);

public slots:
    void stopLogging(QString message);
    void loggingTimer(QString message);

private slots:
    void on_Connect_clicked();
    void on_Disconnect_clicked();
    void on_SampleInterval_textChanged(const QString &arg1);
    void on_TimeUnits_currentIndexChanged(int index);
    void update_SampleRate();
    void on_SetChannels_clicked();
    void on_Start_clicked();
    void on_Stop_clicked();

private:
    Ui::PicoLogger *ui;
    short status; // Holds the picoscope device handle
    short ChanA, ChanB, ChanC, ChanD;
    unsigned long sampleInterval, maxSamples, bufferSize;
    short timeInterval;
    short timeUnit;
    short loggingDurationVal;
    QThread* workerThread;
    double timeUnits[6] = {1e-15, 1e-12, 1e-9, 1e-6, 1e-3, 1};
    short rangeA, rangeB, rangeC, rangeD;
    std::string filename;
    QString bufferSizeText, maxSamplesText;
};

#endif // PICOLOGGER_H
