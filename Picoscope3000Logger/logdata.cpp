#include "logdata.h"
#include <QtCore>
#include <ctime>
#include <string>
#include <iomanip>
#include "picologger.h"
#include <ps3000.h>
#include <fstream>
#include <QDataStream>

// Constructor
logData::logData(){
    noOfSamplesPerAggregate = 1;
}

// Deconstructor
logData::~logData(){

}

/***************************************************
 *  Declare non-member variables and functions
 *
 * ************************************************/
signed short *buffer;
unsigned int samplesPulled;

/*********************************
 *  Get data buffer from device  *
 * ******************************/
// This is the callback function
// On windows, add __stdcall to getBuffer
void getBuffer(short **, short, unsigned int, short, short, unsigned int);
void getBuffer(short ** overviewBuffers,
                        short overflow, unsigned int triggeredAt,
                        short triggered, short auto_stop,
                        unsigned int nValues ){
    //Write device buffer to disk
    // Save the samples pulled per channel
    samplesPulled = nValues;

    for(unsigned int i = 0; i < samplesPulled; i++){
        buffer[i*4] = overviewBuffers[0][i];
        buffer[i*4 + 1] = overviewBuffers[2][i];
        buffer[i*4 + 2] = overviewBuffers[4][i];
        buffer[i*4 + 3] = overviewBuffers[6][i];
    }
}

/***************************************************
 * FUNCTION FOR LOGGING STREAMING DATA
 * Writes directly to binary file during logging
 * *************************************************/
void logData::startLogging( short handle, unsigned long sampleInterval,
                   short timeUnit, unsigned long max_samples,
                   unsigned long overview_buffer_size, short loggingDuration,
                            PicoLogger* parentObject,
                            short rangeA, short rangeB, short rangeC, short rangeD){
    // Create file name
    double fs = 1/(timeUnits[short (timeUnit)]*sampleInterval);
    std::string fsChar = std::to_string(long (fs));
    std::string fileName("Picoscope3000_" + fsChar + "Hz_" + timestampStr() + ".bin");

    buffer = new short[overview_buffer_size];

    /****************************
     *  OPEN STREAM
     * Little endian, single precision (64 bit precision)
     * **************************/
    QFile file(fileName.c_str());
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);
    out.setByteOrder(QDataStream::LittleEndian);
    out.setFloatingPointPrecision(QDataStream::SinglePrecision);
    // Write sample rate as first value
    out << fs;

    // Start streaming
    runStatus = ps3000_run_streaming_ns (handle, uint32_t (sampleInterval),
                                    PS3000_TIME_UNITS(timeUnit), uint32_t (max_samples),
                                    0, uint32_t (noOfSamplesPerAggregate),
                                    uint32_t (overview_buffer_size));
    if(runStatus == 1){
        parentObject->loopLogging = true; // initialize loop condition
        clock_gettime(CLOCK_MONOTONIC, &startTime);
        emit loggingTimer(QString::fromStdString(("Logging starting...")));
    }else{
        logData::stopLogging(QString("Error initializing streaming.  Try lowering max samples."));
        parentObject->loopLogging = false;
        return;
    }

    // Loop pulling data from buffer
    while (parentObject->loopLogging == true){
        // Pull data from buffer
        samplesPulled = 0;
        streamingStatus = ps3000_get_streaming_last_values(handle,getBuffer);
        // Convert buffer to voltage and write to file
        for(short i = short (0); i < short (samplesPulled*4); i+=4){
            out << double (buffer[i]) * voltages[rangeA] / PS3000_MAX_VALUE;
            out << double (buffer[i+1]) * voltages[rangeB] / PS3000_MAX_VALUE;
            out << double (buffer[i+2]) * voltages[rangeC] / PS3000_MAX_VALUE;
            out << double (buffer[i+3]) * voltages[rangeD] / PS3000_MAX_VALUE;
        }

        // Calculate seconds elapsed
        clock_gettime(CLOCK_MONOTONIC, &currentTime);
        elapsedTime = (currentTime.tv_sec - startTime.tv_sec);

        // Check for buffer overload
        bufferOverrunStatus = ps3000_overview_buffer_status(handle, &bufferOverrun);

        if(bufferOverrunStatus == 0){
            // Check device status
            stopMessage = QString("Buffer Error: Invalid device handle.");
            parentObject->loopLogging = false;
        }else if (bufferOverrun != 0){
            // Check buffer status
            stopMessage = (QString("Buffer overload.  Increase buffer size."));
            parentObject->loopLogging = false;
        }else if(elapsedTime >= loggingDuration && loggingDuration > 0){
            // Check if timer is done
            parentObject->loopLogging = false;
            stopMessage = QString("Logging Finished");
        }else{
            emit loggingTimer(QString::fromStdString(("Logging for " + std::to_string(elapsedTime ) + " seconds")));
            stopMessage = QString("Logging Stopped");
        }
    }
    /******************************
     * Stop logging and close file
     ******************************/
    // Send stop signal to device
    closingHandle = ps3000_stop(handle);

    if(closingHandle == 1){
        stopMessage = stopMessage + QString("; Device stopped");
    }else{
        stopMessage = stopMessage + QString("; Error, Device could not stop");
    }

    // Close file writing stream and delete application buffer array
    file.close();
    delete[] buffer;
    emit stopLogging(stopMessage);
}

/*****************************
 * Write time stamp
 *****************************/
std::string logData::timestampStr(){
    std::time_t rawtime;
    struct tm * timeinfo;
    char dataBuf[80];

    std::time(&rawtime); //get system tume
    timeinfo = std::localtime(&rawtime); // convert ot local time

    std::strftime(dataBuf,sizeof(dataBuf),"%Y-%m-%d_%H-%M-%S",timeinfo);
    // Return a timestamp as a string
    return std::string(dataBuf);
}
