#ifndef LOGDATA_H
#define LOGDATA_H
#include<QtCore>
#include <string>
#include "picologger.h"

class logData : public QObject
{
    Q_OBJECT


private:
    struct timespec startTime, currentTime;
    long elapsedTime;
    short runStatus;
    short bufferOverrun;
    short bufferOverrunStatus;
    unsigned int noOfSamplesPerAggregate;
    QString stopMessage;
    double voltages[15] = {0.01, 0.02, 0.05, 0.1, 0.2, 0.5,
                           1, 2, 5, 10, 20, 50, 100, 200, 400};
    double timeUnits[6] = {1e-15, 1e-12, 1e-9, 1e-6, 1e-3, 1};
    std::string timestampStr();
    short streamingStatus;
    short closingHandle;


public:
    logData();
    ~logData();

public slots:
    void startLogging(short, unsigned long, short,
                      unsigned long, unsigned long,
                      short, PicoLogger*,
                      short, short, short, short);

signals:
    int stopLogging(const QString stopMessage);
    int loggingTimer(const QString elapsedTime);
};

#endif // LOGDATA_H
