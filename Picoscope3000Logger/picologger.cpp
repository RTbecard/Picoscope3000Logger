#include "picologger.h"
#include "ui_picologger.h"
#include <ps3000.h>
#include <QMessageBox>
#include <QPushButton>
#include "logdata.h"
#include <string>

logData* worker;

PicoLogger::PicoLogger(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PicoLogger)
{
    ui->setupUi(this);
}

PicoLogger::~PicoLogger()
{
    PicoLogger::stopLogging(QString("Closing Program..."));
    short statusDisconnect = ps3000_close_unit(status);
    delete ui;
}

void PicoLogger::on_Connect_clicked()
{
    QMainWindow::statusBar()->showMessage("Searching for devices...");
    status = ps3000_open_unit();
    QString statusChar = QString::number (status);
    if(status > 0){
        QMainWindow::statusBar()->showMessage("Oscilloscope Connected. Device: " + statusChar);
        ui->Connect->setEnabled(false);
        ui->Disconnect->setEnabled(true);
        ui->Start->setEnabled(false);
        ui->Stop->setEnabled(false);
        ui->SetChannels->setEnabled(true);

        ui->ChannelA->setEnabled(true);
        ui->ChannelB->setEnabled(true);
        ui->ChannelC->setEnabled(true);
        ui->ChannelD->setEnabled(true);

        ui->BufferSize->setEnabled(true);
        ui->MaxSamples->setEnabled(true);
        ui->SampleInterval->setEnabled(true);
        ui->TimeUnits->setEnabled(true);

        ui->SetChannels->setEnabled(true);

        ui->Start->setEnabled(false);
        ui->Stop->setEnabled(false);
    }else{
        QMainWindow::statusBar()->showMessage("Could not connect to oscilliscope.  Device: " + statusChar);
        ui->Connect->setEnabled(true);
        ui->Disconnect->setEnabled(false);
    }
}

void PicoLogger::on_Disconnect_clicked()
{
    short statusDisconnect = ps3000_close_unit(status);
    if(statusDisconnect == 1){
        QString statusChar = QString::number (status);
        QMainWindow::statusBar()->showMessage("Oscilloscope Disconnected. Device: " + statusChar);
        ui->Connect->setEnabled(true);
        ui->ChannelA->setEnabled(false);
        ui->ChannelB->setEnabled(false);
        ui->ChannelC->setEnabled(false);
        ui->ChannelD->setEnabled(false);

        ui->BufferSize->setEnabled(false);
        ui->MaxSamples ->setEnabled(false);
        ui->SampleInterval->setEnabled(false);
        ui->TimeUnits->setEnabled(false);

        ui->SetChannels->setEnabled(false);
    }else{
        QMainWindow::statusBar()->showMessage("No Device Found.");
        QString statusChar = QString::number (status);
        ui->Connect->setEnabled(true);
        ui->ChannelA->setEnabled(false);
        ui->ChannelB->setEnabled(false);
        ui->ChannelC->setEnabled(false);
        ui->ChannelD->setEnabled(false);

        ui->BufferSize->setEnabled(false);
        ui->MaxSamples->setEnabled(false);
        ui->SampleInterval->setEnabled(false);
        ui->TimeUnits->setEnabled(false);

        ui->SetChannels->setEnabled(false);
    }
}


void PicoLogger::update_SampleRate(){
    timeUnit = ui->TimeUnits->currentIndex();
    sampleInterval = ui->SampleInterval->text().toDouble();

    double sampleRate = 1/(timeUnits[(short) timeUnit]*sampleInterval);
    ui->SampleRate->setText(QString::number(sampleRate));
}

void PicoLogger::on_SampleInterval_textChanged(const QString &arg1)
{
    update_SampleRate();
}

void PicoLogger::on_TimeUnits_currentIndexChanged(int index)
{
    update_SampleRate();
}

void PicoLogger::on_SetChannels_clicked()
{
    // Grab values from UI
    rangeA = ui->ChannelA->currentIndex() + 1;
    rangeB = ui->ChannelB->currentIndex() + 1;
    rangeC = ui->ChannelC->currentIndex() + 1;
    rangeD = ui->ChannelD->currentIndex() + 1;
    bufferSizeText = ui->BufferSize->text(); bufferSize = bufferSizeText.toULong();
    maxSamplesText = ui->MaxSamples->text();maxSamples = maxSamplesText.toULong();
    timeUnit = ui->TimeUnits->currentIndex();
    sampleInterval = ui->SampleInterval->text().toULong();

    short channelStatus;
    channelStatus = ps3000_set_channel ( status, 0, true, false, rangeA);
    channelStatus += ps3000_set_channel ( status, 1, true, false, rangeB);
    channelStatus += ps3000_set_channel ( status, 2, true, false, rangeC);
    channelStatus += ps3000_set_channel ( status, 3, true, false, rangeD);
    if (channelStatus < 4){
        QMainWindow::statusBar()->showMessage("Error setting one or more channels. Check voltage ranges.");
        ui->Start->setEnabled(false);
        ui->Stop->setEnabled(false);
    }else{
        QMainWindow::statusBar()->showMessage("Channels Set.");
        ui->Start->setEnabled(true);
        ui->Stop->setEnabled(false);

        ui->ChannelA->setEnabled(false);
        ui->ChannelB->setEnabled(false);
        ui->ChannelC->setEnabled(false);
        ui->ChannelD->setEnabled(false);

        ui->BufferSize->setEnabled(false);
        ui->MaxSamples->setEnabled(false);
        ui->SampleInterval->setEnabled(false);
        ui->TimeUnits->setEnabled(false);

        ui->SetChannels->setEnabled(false);
    }
}
/**************************
 *  START LOGGING
 *  Opens new worker thread and sends
 *  signal to start logging data
 * ***********************/
void PicoLogger::on_Start_clicked()
{
    // Initialize new logData object
    worker = new logData();
    // Move to worker thread
    QThread* thread = new QThread;
    workerThread = thread;
    worker->moveToThread(thread);

    // Link signal/slots between threads
    QObject::connect(this ,
                     SIGNAL(startLogging(short, unsigned long, short,
                                         unsigned long, unsigned long,
                                         short, PicoLogger*,
                                         short, short, short, short)),
                     worker,
                     SLOT(startLogging(short, unsigned long, short,
                                                unsigned long, unsigned long,
                                       short, PicoLogger*,
                                       short, short, short, short)));
    QObject::connect(worker ,SIGNAL(stopLogging(QString)), this,
                     SLOT(stopLogging(QString)));
    QObject::connect(worker,SIGNAL(loggingTimer(QString)), this,
                     SLOT(loggingTimer(QString)));

    // Start worker thread
    thread->setObjectName(QString("PicoLogger_Streaming"));
    thread->start();
    thread->setPriority(QThread::HighPriority);

    // Get data from UI
    QString loggingDurationText = ui->LoggingTime->text();
    loggingDurationVal = loggingDurationText.toShort();



    // Send signal for starting logging
    ui->Start->setEnabled(false);
    ui->Stop->setEnabled(true);
    emit startLogging(status, sampleInterval,timeUnit,
                      maxSamples, bufferSize, loggingDurationVal,
                      this, rangeA, rangeB, rangeC, rangeD);
}

/**************************
 *  STOP LOGGING
 *  Closes worker thread and updates UI
 * ***********************/
void PicoLogger::stopLogging(QString message)
{
    ps3000_stop(status);
    ui->statusBar->showMessage(message);
    ui->Start->setEnabled(true);
    //delete worker;
    workerThread->exit();
    delete worker;

}

/**************************
 *  UPDATE TIMER
 *  Shows seconds data has been logged for
 * ***********************/
void PicoLogger::loggingTimer(QString message)
{
    ui->statusBar->showMessage(message);
}

void PicoLogger::on_Stop_clicked()
{
    loopLogging = false;
    ui->Start->setEnabled(true);
}
