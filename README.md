# Picologger3000
A cross-platform fast streaming logger for Picoscope 3000 series oscilliscopes built with Qt .  


This app returns binary files saved as single precision float values.
The first value is the sample rate, and the remaining values are the absolute voltage for each of the 4 channels.
You can also use the function `read.PicoFile.bin` from the package [LUAnimBehav](https://gitlab.com/RTbecard/LUAnimBehav) to read this data directly into R for quick analysis.

![Screen](/screen1.jpeg)

## Usage

You can build from source, or grab the binaries I compiled under the following systems:

- [Ubuntu 16.04, 64bit, version 1.1.1](https://www.dropbox.com/s/kelejvt8q52hi6r/Picoscope3000Logger_Ubuntu_1.1.1?dl=0)
- [Windows 10, 32bit, version 1.1.1](https://www.dropbox.com/s/kgas0no760u4kj5/Picoscope3000Logger_Windows_1.1.1.exe?dl=0)

```bash
# Add repo to sources
sudo bash -c 'echo "deb https://labs.picotech.com/debian/ picoscope main" >/etc/apt/sources.list.d/picoscope.list'
# Import public key
wget -O - https://labs.picotech.com/debian/dists/picoscope/Release.gpg.key | sudo apt-key add -

# Update package manager cache
sudo apt-get update

# Install Drivers for 3000 series (discontinued)
sudo apt install libps3000
```

...while on **windows 10** you'll have to **download and install the [32-bit Picoscope 3425 SDK](https://www.picotech.com/downloads/_lightbox/pico-software-development-kit-32-bit)** to the default install directory.
This will load the device drivers and the Picologger will assume the files are installed to the default install directory on c drive.

## Source Code

To compile on windows or ubuntu, please select the branches named *windows* and *ubuntu* respectively.


## Issues

I've previously ran into issues with the device crashing upon repeated logging sessions on Ubuntu 16.04.
The workaround for this is to disconnect and reconnect the device after each logging session.
This is likely due to a bug in the PS3000 drivers.

## License
As this was built with the open source edition of Qt, this is licensed under the GPL-3 license.
